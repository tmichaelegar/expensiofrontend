import React from 'react';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import Route from 'react-router-dom/Route';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


// components
import NavBar from './components/NavBar.js';
import Welcome from './welcome_page/WelcomePage';
import Login from './login_page/Login';
import Register from './register_page/Register';


function App() {
  return (
    <div className="App">
      <div className="Nav-bar">
        <NavBar />
      </div>
      <div className="Viewing-Component">
        <BrowserRouter>
          <div>
            <Route path="/" component={Welcome} exact/>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </div>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
